package com.example.portfolio.Service;

import com.example.portfolio.Model.Contacto;
import com.example.portfolio.Model.Personal;
import com.example.portfolio.Repository.ContactoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContactoService {
    @Autowired
    private ContactoRepository contactoRepository;

    public List<Contacto> getAllContacto() {
        return contactoRepository.findAll();
    }


}
