package com.example.portfolio.Service;

import com.example.portfolio.Model.Lenguaje;
import com.example.portfolio.Model.Personal;
import com.example.portfolio.Repository.LenguajeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LenguajeService {
    @Autowired
    private LenguajeRepository lenguajeRepository;
    public List<Lenguaje> getAllLenguaje() {
        return lenguajeRepository.findAll();
    }
}
