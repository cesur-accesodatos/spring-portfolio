package com.example.portfolio.Service;

import com.example.portfolio.Model.Personal;
import com.example.portfolio.Repository.PersonalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonalService {
    @Autowired
    private PersonalRepository personalRepository;

    public List<Personal> getAllPersonal() {
        return personalRepository.findAll();
    }

}
