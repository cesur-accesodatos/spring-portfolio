package com.example.portfolio.Service;

import com.example.portfolio.Model.Personal;
import com.example.portfolio.Model.Proyecto;
import com.example.portfolio.Repository.ProyectoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProyectoService {
    @Autowired
    private ProyectoRepository proyectoRepository;
    public List<Proyecto> getAllProyecto() {
        return proyectoRepository.findAll();
    }

}
