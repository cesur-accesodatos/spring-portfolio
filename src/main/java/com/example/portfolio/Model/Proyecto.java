package com.example.portfolio.Model;

import jakarta.persistence.*;

@Entity
@Table(name = "proyecto")
public class Proyecto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String urlP;
    private int lenguaje_id;

    public String getUrlP() {
        return urlP;
    }

    public void setUrl(String urlP) {
        this.urlP = urlP;
    }

    public int getLenguaje_id() {
        return lenguaje_id;
    }

    public void setLenguaje_id(int lenguaje_id) {
        this.lenguaje_id = lenguaje_id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
