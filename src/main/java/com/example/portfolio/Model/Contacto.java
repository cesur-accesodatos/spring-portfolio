package com.example.portfolio.Model;

import jakarta.persistence.*;

@Entity
@Table(name = "contacto")
public class Contacto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nombre;
    private String email;
    private String mensaje;
    private String correo;
    private String linkedin;
    private String insta;
    private String imgcorreo;
    private String imglinkedin;
    private String imginsta;

    public String getImgcorreo() {
        return imgcorreo;
    }

    public void setImgcorreo(String imgcorreo) {
        this.imgcorreo = imgcorreo;
    }

    public String getImglinkedin() {
        return imglinkedin;
    }

    public void setImglinkedin(String imglinkedin) {
        this.imglinkedin = imglinkedin;
    }

    public String getImginsta() {
        return imginsta;
    }

    public void setImginsta(String imginsta) {
        this.imginsta = imginsta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public String getInsta() {
        return insta;
    }

    public void setInsta(String insta) {
        this.insta = insta;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
