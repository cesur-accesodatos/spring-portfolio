package com.example.portfolio.Repository;

import com.example.portfolio.Model.Lenguaje;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LenguajeRepository extends JpaRepository<Lenguaje,Long> {
}
