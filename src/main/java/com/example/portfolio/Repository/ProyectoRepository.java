package com.example.portfolio.Repository;

import com.example.portfolio.Model.Proyecto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProyectoRepository extends JpaRepository<Proyecto,Long> {
}
