package com.example.portfolio.Repository;

import com.example.portfolio.Model.Contacto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactoRepository extends JpaRepository<Contacto,Long> {
}
