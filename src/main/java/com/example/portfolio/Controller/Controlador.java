package com.example.portfolio.Controller;

import com.example.portfolio.Model.Proyecto;
import com.example.portfolio.Service.ContactoService;
import com.example.portfolio.Service.LenguajeService;
import com.example.portfolio.Service.PersonalService;
import com.example.portfolio.Service.ProyectoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;


@Controller
public class Controlador {
    @Autowired
    private ProyectoService proyectoService;
    @Autowired
    private ContactoService contactoService;
    @Autowired
    private LenguajeService lenguajeService;
    @Autowired
    private PersonalService personalService;

    @RequestMapping("/")
    String hola(Model model) {
        model.addAttribute("proyecto",proyectoService.getAllProyecto());
        model.addAttribute("contacto",contactoService.getAllContacto());
        model.addAttribute("lenguaje",lenguajeService.getAllLenguaje());
        model.addAttribute("personal",personalService.getAllPersonal());
        return "index";
    }
}
