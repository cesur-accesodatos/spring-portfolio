drop database portfolio;
create database portfolio;
use portfolio;

Create table personal (
id int auto_increment primary key,
puesto varchar(255),
 nombre varchar(255),
 img varchar(255),
 descripcion varchar(1000)
);

create table lenguaje (
id int auto_increment primary key,
url varchar (255),
nombre varchar (255),
nivel int CHECK(nivel BETWEEN 1 AND 5)
);

create table proyecto (
id int auto_increment primary key,
urlP varchar (255),
lenguaje_id int-- ,
-- foreign key (lenguaje_id) references lenguaje(id)
);

create table contacto (
id int auto_increment primary key,
nombre varchar (255),
email varchar (255),
mensaje varchar (1000),
correo varchar(255),
linkedin varchar(255),
insta varchar(255)
);

insert personal (puesto,nombre,img,descripcion) values ("software developer", "Yixin", "/css/img/foto.png",
				"Soy un desarrollador motivado y con ganas de aprender. Estoy siempre buscando nuevas
                oportunidades para mejorar mis habilidades y conocimientos. Actualmente estoy estudiando en
                Cesur, y anteriormente estuve estudiando ingeniería informática en la universidad Nebrija. Me
                centro en general y preferiblemente en el back-end, pero si se me ofrece una oferta sobre el
                front-end, no tendría ningún problema en hacerlo. Me gusta variar y enfrentar a distintos retos.");
insert lenguaje (url,nombre,nivel) values("/css/img/java.png","Java",4);
insert lenguaje (url,nombre,nivel) values("/css/img/kotlin.png","Kotlin",2);
insert lenguaje (url,nombre,nivel) values("/css/img/csharp.png","c#",2);
insert lenguaje (url,nombre,nivel) values("/css/img/mysql.png","MySQL",3);
insert lenguaje (url,nombre,nivel) values("/css/img/html.png","HTML",3);
insert lenguaje (url,nombre,nivel) values("/css/img/css.png","CSS",3);

insert proyecto (urlP,lenguaje_id) values("/css/img/ejemplo1.png",1);
insert proyecto (urlP,lenguaje_id) values("/css/img/ejemplo2.png",1);
insert proyecto (urlP,lenguaje_id) values("/css/img/ejemplo3.png",5);
insert proyecto (urlP,lenguaje_id) values("/css/img/ejemplo4.png",5);

insert contacto (correo,linkedin,insta) values("mailto:yixin.s358794@cesurformacion.com","https://www.linkedin.com/in/yixin-xia-wang/","https://www.instagram.com/yiiixiiin_xiiia/");

select * from personal;
select * from proyecto;
show tables;